#include <pthread.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

double sum;
double frob_sq;
double **A;
double **B;
double **C;
int na;
int nb;
pthread_mutex_t mymutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mymutexAB = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mymutexC = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mymutex2 = PTHREAD_MUTEX_INITIALIZER;

typedef pthread_t *PT;
typedef struct multiply_args {
  int *indices;
} *MA;
int **partition (int length, int n) {
  int i;
  int j;
  double division = length / (double)n;
  int **result = malloc (n * sizeof (int*));
  for (i = 0; i < n; ++i) {
    int index_ini = (int)(division * i + 0.5); // 2
    int index_fin = (int)(division * (i + 1) + 0.5); // 5
    int m = index_fin - index_ini;
    result[i] = malloc ((m + 1) * sizeof (int));
    result[i][0] = m;
    for (j = 1; j < m + 1; ++j) {
      result[i][j] = index_ini + j - 1;
    }
  }
  return result;
}
void *multiply (void *arg) {
  // Funkcja mnoży część macierzy A o wymiarach am x an
  // i B o wymiarach (bm x bn), taką że obliczone zostaną
  // elementy macierzy C = AB o indeksach między cm_min a cm_max
  // oraz między cn_min a cm_max
  MA args = arg;
  int *indices = args->indices;
  int n = indices[0];
  //printf ("%d %d %d %d\n", cm_min, cm_max, cn_min, cn_max);
  int i, j, k, ti, t;
  double s;
  //for (i = 0; i < n; i++) {
  //  printf ("%d ", indices[i + 1]);
  //}
  //printf ("\n");
  for (ti = 0; ti < n; ++ti) {
    t = indices [ti + 1];
    i = t / nb;
    j = t - nb * i;
    //printf ("i=%d j=%d\n", i, j);
    s = 0;
    for (k = 0; k < na; k++) {
      pthread_mutex_lock (&mymutexAB);
      s += A[i][k] * B[k][j];
      pthread_mutex_unlock (&mymutexAB);
    }
    C[i][j] = s;
    pthread_mutex_lock (&mymutex);
    sum += s;
    frob_sq += s * s;
    pthread_mutex_unlock (&mymutex);
  }
  return NULL;
}

void print_matrix (double **M, int m, int n) {
  int i, j;
  printf ("[");
  for (i = 0; i < m; i++) {
    for (j = 0; j < n; j++) {
      printf ("%f ", M[i][j]);
    }
    printf ("\n");
  }
  printf ("]\n");
}
int main (int argc, char **argv) {
  pthread_t **threads;
  char* aname;
  char* bname;
  int **indices;
  if (argc == 3) {
    aname = argv[1];
    bname = argv[2];
  } else if (argc > 3) {
    aname = argv[2];
    bname = argv[3];
  } else {
    aname = "A.txt";
    bname = "B.txt";
  }
  FILE *fpa;
  FILE *fpb;
  int ma, mb;
  int i, j;
  double x;
  int N = argc > 1 & argc != 3 ? atoi (argv[1]) : 1;
  fpa = fopen (aname, "r");
  fpb = fopen (bname, "r");
  if (fpa == NULL || fpb == NULL) {
    perror ("błąd otwarcia pliku");
    exit (-10);
  }

  fscanf (fpa, "%d", &ma);
  fscanf (fpa, "%d", &na);


  fscanf (fpb, "%d", &mb);
  fscanf (fpb, "%d", &nb);

  printf ("pierwsza macierz ma wymiar %d x %d, a druga %d x %d\n", ma, na, mb, nb);

  if (na != mb) {
    printf ("Złe wymiary macierzy!\n");
    return EXIT_FAILURE;
  }
  /*Alokacja pamięci*/
  A = malloc (ma * sizeof (double*));
  for (i = 0; i < ma; i++) {
    A[i] = malloc (na * sizeof (double));
  }

  B = malloc (mb * sizeof (double*));
  for (i = 0; i < mb; i++) {
    B[i] = malloc (nb * sizeof (double));
  }

  /*Macierz na wynik*/
  C = malloc (ma * sizeof (double*));
  for (i = 0; i < ma; i++) {
    C[i] = malloc (nb * sizeof (double));
  }

  printf ("Rozmiar C: %dx%d\n", ma, nb);
  for (i = 0; i < ma; i++) {
    for (j = 0; j < na; j++) {
      fscanf (fpa, "%lf", &x);
      A[i][j] = x;
    }
  }

  printf ("A:\n");
  //print_matrix (A, ma, mb);

  for (i = 0; i < mb; i++) {
    for(j = 0; j < nb; j++) {
      fscanf (fpb, "%lf", &x);
      B[i][j] = x;
    }
  }

  printf ("B:\n");
  //print_matrix (B, mb, nb);

  //printf ("N = %d\n", N);
  if (N > ma * nb) {
    N = ma * nb;
  }
  indices = partition (ma * nb, N);
  threads = malloc (N * sizeof (pthread_t*));
  MA margs = malloc (N * sizeof (struct multiply_args));
  for (i = 0; i < N; i++) {
    margs[i].indices = indices[i];
    threads[i] = malloc (sizeof (pthread_t));
    pthread_attr_t attr;
    pthread_attr_init (&attr);
    if (pthread_create (threads[i], &attr, multiply, &margs[i])) {
      printf ("error creating thread.");
      abort ();
    }
  }
  printf ("Liczba utworzonych wątków: %d\n", N);
  for (i = 0; i < N; i++) {
    pthread_join (*threads[i], NULL);
  }
  free (margs);
  printf ("C:\n");
  //print_matrix (C, ma, nb);
  printf ("Suma elementów macierzy C: %f\n", sum);
  printf ("Norma Frobeniusa macierzy C: %f\n", sqrt (frob_sq));


  for (i = 0; i < ma; i++) {
    free (A[i]);
  }
  free (A);

  for (i = 0; i < mb; i++) {
    free (B[i]);
  }
  free (B);

  for (i = 0; i < ma; i++) {
    free (C[i]);
  }
  free (C);

  for (i = 0; i < N; i++) {
    free (indices[i]);
  }
  free (indices);
   
  fclose (fpa);
  fclose (fpb);
  
  for (i = 0; i < N; i++) {
    free (threads[i]);
  }
  free (threads);

  return 0;
}
